MODULE lib_rbt
  !   A FORTRAN Implementation of Red-Black Tree Translated from CLRS.
  IMPLICIT NONE

  PRIVATE
  PUBLIC :: RBNODE_T, RBTREE_T
  PUBLIC :: MINVL
  PUBLIC :: rbt_setup, rbt_free
  PUBLIC :: rbt_insert, rbt_delete
  PUBLIC :: rbt_prev, rbt_next
  PUBLIC :: rbt_upper_bound, rbt_get_value

  REAL(8), PARAMETER :: MINVL = -HUGE(0.0D0)

  ! define node colors
  LOGICAL, PARAMETER :: RED = .TRUE.
  LOGICAL, PARAMETER :: BLACK = .FALSE.

  ! define node structure
  TYPE :: RBNODE_T
    ! key
    REAL(8) :: key = MINVL
    ! value
    REAL(8) :: val = MINVL
    ! node color
    LOGICAL :: color = BLACK
    ! pointer to parent node
    TYPE(RBNODE_T), POINTER :: parent => NULL()
    ! pointer to left child node
    TYPE(RBNODE_T), POINTER :: left => NULL()
    ! pointer to right child node
    TYPE(RBNODE_T), POINTER :: right => NULL()
  END TYPE RBNODE_T
  ! sentinel target
  TYPE(RBNODE_T), TARGET :: NIL
  ! memory pool
  TYPE(RBNODE_T), ALLOCATABLE, TARGET :: nodes(:)

  ! define red-black tree class
  TYPE :: RBTREE_T
    ! pointer to root rb-node
    TYPE(RBNODE_T), POINTER :: root => NULL()
    ! null pointer
    TYPE(RBNODE_T), POINTER :: nil => NULL()
    ! pointer to the minimum key
    TYPE(RBNODE_T), POINTER :: first => NULL()
    ! pointer to the maximum key
    TYPE(RBNODE_T), POINTER :: last => NULL()
    ! size of rb-tree
    INTEGER :: tsize = 0
    ! current maximal size of rb-tree
    INTEGER :: max_size = 1024
  CONTAINS
    PROCEDURE :: setup => rbt_setup
    PROCEDURE :: get_value => rbt_get_value
    PROCEDURE :: insert => rbt_insert
    PROCEDURE :: delete => rbt_delete
    PROCEDURE :: upper_bound => rbt_upper_bound
    PROCEDURE :: prev => rbt_prev
    PROCEDURE :: next => rbt_next
    PROCEDURE :: new_node => rbt_new_node
    final :: rbt_free
  END TYPE RBTREE_T


CONTAINS
  !----------------------------------------------------------------------------
  SUBROUTINE rbt_setup(rbt_)
    IMPLICIT NONE

    CLASS(RBTREE_T), INTENT(INOUT) :: rbt_

    rbt_%root => NIL
    rbt_%nil => NIL
    rbt_%first => NIL
    rbt_%last => NIL
    rbt_%tsize = 0
    rbt_%max_size = 1024

    ! allocate a memory pool
    ALLOCATE(nodes(rbt_%max_size))

  END SUBROUTINE rbt_setup
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  SUBROUTINE rbt_free(rbt_)
    IMPLICIT NONE

    TYPE(RBTREE_T), INTENT(INOUT) :: rbt_

    DEALLOCATE(nodes)

    ! IF (ASSOCIATED(rbt_%root)) THEN
    !   CALL free_node_(rbt_%root)
    !   DEALLOCATE(rbt_%root)
    ! END IF

  END SUBROUTINE rbt_free
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  FUNCTION rbt_new_node(rbt_, key_, val_, color_) RESULT(node_)
    IMPLICIT NONE

    CLASS(RBTREE_T), INTENT(INOUT) :: rbt_
    REAL(8), INTENT(IN) :: key_, val_
    LOGICAL, INTENT(IN) :: color_
    TYPE(RBNODE_T), POINTER :: node_
    TYPE(RBNODE_T), ALLOCATABLE, TARGET :: tmp(:)
    INTEGER :: tsize, max_size

    tsize = rbt_%tsize + 1
    max_size = rbt_%max_size
    IF (tsize <= max_size) THEN
      nodes(tsize)%key = key_
      nodes(tsize)%val = val_
      nodes(tsize)%color = color_
      nodes(tsize)%parent => NIL
      nodes(tsize)%left => NIL
      nodes(tsize)%right => NIL
    ELSE
      ALLOCATE(tmp(max_size+256))
      tmp(1:max_size) = nodes(1:max_size)
      CALL MOVE_ALLOC(FROM=tmp, TO=nodes)
      nodes(tsize)%key = key_
      nodes(tsize)%val = val_
      nodes(tsize)%color = color_
      nodes(tsize)%parent => NIL
      nodes(tsize)%left => NIL
      nodes(tsize)%right => NIL
      ! update max_size
      rbt_%max_size = max_size + 256
    END IF
    node_ => nodes(tsize)

  END FUNCTION rbt_new_node
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  FUNCTION rbt_get_value(rbt_, key_) RESULT(val_)
    IMPLICIT NONE

    CLASS(RBTREE_T), INTENT(INOUT) :: rbt_
    REAL(8), INTENT(IN) :: key_
    REAL(8) :: val_
    TYPE(RBNODE_T), POINTER :: p_

    p_ => rbt_%root
    DO WHILE(.NOT. ASSOCIATED(p_, NIL))
      IF (key_ < p_%key) THEN
        p_ => p_%left
      ELSE IF (key_ > p_%key) THEN
        p_ => p_%right
      ELSE
        val_ = p_%val
        RETURN
      END IF
    END DO
    val_ = MINVL

    RETURN
  END FUNCTION rbt_get_value
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  SUBROUTINE rbt_insert(rbt_, key_, val_)
    IMPLICIT NONE

    CLASS(RBTREE_T), INTENT(INOUT) :: rbt_
    REAL(8), INTENT(IN) :: key_, val_
    TYPE(RBNODE_T), POINTER :: node_

    ! node_ => new_node_(key_, val_, RED)
    node_ => rbt_%new_node(key_, val_, RED)
    CALL insert_(rbt_%root, node_)

    IF (ASSOCIATED(rbt_%root, node_)) THEN
      rbt_%first => rbt_%root
      rbt_%last => rbt_%root
    ELSE
      IF (node_%key < rbt_%first%key) THEN
        rbt_%first => node_
      END IF
      IF (node_%key > rbt_%last%key) THEN
        rbt_%last => node_
      END IF
    END IF
    !
    rbt_%tsize = rbt_%tsize + 1

  END SUBROUTINE rbt_insert
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  SUBROUTINE rbt_delete(rbt_, key_)
    IMPLICIT NONE

    CLASS(RBTREE_T), INTENT(INOUT) :: rbt_
    REAL(8), INTENT(IN) :: key_
    TYPE(RBNODE_T), POINTER :: node_, p_

    node_ => find_(rbt_%root, key_)
    IF (.NOT. ASSOCIATED(node_, NIL)) THEN
      IF (ASSOCIATED(node_, rbt_%first)) THEN
        rbt_%first => node_%parent
      END IF
      IF (ASSOCIATED(node_, rbt_%last)) THEN
        rbt_%last => node_%parent
      END IF

      CALL delete_(rbt_%root, node_)
      ! DEALLOCATE(node_)

      ! transfer node at nodes(tsize) to the
      ! position of node to be deleted
      p_ => nodes(rbt_%tsize)
      node_ = nodes(rbt_%tsize)
      p_%left%parent => node_
      p_%right%parent => node_
      IF (.NOT. ASSOCIATED(p_%parent, NIL)) THEN
        IF (ASSOCIATED(p_%parent%left, p_)) THEN
          p_%parent%left => node_
        ELSE
          p_%parent%right => node_
        END IF
      END IF
      !
      rbt_%tsize = rbt_%tsize - 1
    END IF

  END SUBROUTINE rbt_delete
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  FUNCTION rbt_upper_bound(rbt_, x_) RESULT(ub_)
    IMPLICIT NONE

    CLASS(RBTREE_T), INTENT(INOUT) :: rbt_
    REAL(8), INTENT(IN) :: x_
    TYPE(RBNODE_T), POINTER :: ub_
    TYPE(RBNODE_T), POINTER :: p_

    IF (x_ < rbt_%first%key) THEN
      ub_ => rbt_%first
      RETURN
    END IF

    p_ => rbt_%root
    DO WHILE (.NOT. ASSOCIATED(p_, NIL))
      IF (x_ > p_%key) THEN
        p_ => p_%right
      ELSE IF (x_ < p_%key) THEN
        IF (x_ >= p_%left%key) THEN
          ub_ => p_
          RETURN
        ELSE
          p_ => p_%left
        END IF
      ELSE
        ub_ => p_%right
      END IF
    END DO
    ub_ => NIL

    RETURN
  END FUNCTION rbt_upper_bound
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  FUNCTION rbt_prev(rbt_, x_) RESULT(prev_)
    IMPLICIT NONE

    CLASS(RBTREE_T), INTENT(INOUT) :: rbt_
    TYPE(RBNODE_T), POINTER :: x_
    TYPE(RBNODE_T), POINTER :: prev_

    IF (ASSOCIATED(x_, rbt_%root)) THEN
      prev_ => x_%left
      RETURN
    END IF

    IF (.NOT. ASSOCIATED(x_%left, NIL)) THEN
      prev_ => x_%left
    ELSE IF (x_%key > x_%parent%key) THEN
      prev_ => x_%parent
    ELSE
      prev_ => NIL
    END IF

    RETURN
  END FUNCTION rbt_prev
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  FUNCTION rbt_next(rbt_, x_) RESULT(next_)
    IMPLICIT NONE

    CLASS(RBTREE_T), INTENT(INOUT) :: rbt_
    TYPE(RBNODE_T), POINTER :: x_
    TYPE(RBNODE_T), POINTER :: next_

    IF (ASSOCIATED(x_, rbt_%root)) THEN
      next_ => x_%right
      RETURN
    END IF

    IF (.NOT. ASSOCIATED(x_%right, NIL)) THEN
      next_ => x_%right
    ELSE IF (x_%key < x_%parent%key) THEN
      next_ => x_%parent
    ELSE
      next_ => NIL
    END IF

    RETURN
  END FUNCTION rbt_next
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  SUBROUTINE insert_(root_, z_)
    IMPLICIT NONE

    TYPE(RBNODE_T), POINTER, INTENT(INOUT) :: root_
    TYPE(RBNODE_T), POINTER, INTENT(INOUT) :: z_
    TYPE(RBNODE_T), POINTER :: x_, y_

    y_ => NIL
    x_ => root_

    DO WHILE (.NOT. ASSOCIATED(x_, NIL))
      y_ => x_
      IF (z_%key < x_%key) THEN
        x_ => x_%left
      ELSE
        x_ => x_%right
      END IF
    END DO

    z_%parent => y_
    IF (ASSOCIATED(y_, NIL)) THEN
      root_ => z_
    ELSE IF (z_%key < y_%key) THEN
      y_%left => z_
    ELSE
      y_%right => z_
    END IF

    CALL insert_fixup_(root_, z_)

  END SUBROUTINE insert_
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  SUBROUTINE insert_fixup_(root_, z_)
    IMPLICIT NONE

    TYPE(RBNODE_T), POINTER, INTENT(INOUT) :: root_
    TYPE(RBNODE_T), POINTER, INTENT(INOUT) :: z_
    TYPE(RBNODE_T), POINTER :: y_

    DO WHILE (z_%parent%color .EQV. RED)
      IF (ASSOCIATED(z_%parent, z_%parent%parent%left)) THEN
        y_ => z_%parent%parent%right
        IF (y_%color .EQV. RED) THEN
          z_%parent%color = BLACK
          y_%color = BLACK
          z_%parent%parent%color = RED
          z_ => z_%parent%parent
        ELSE
          IF (ASSOCIATED(z_, z_%parent%right)) THEN
            z_ => z_%parent
            CALL left_rotate_(root_, z_)
          END IF
          z_%parent%color = BLACK
          z_%parent%parent%color = RED
          CALL right_rotate_(root_, z_%parent%parent)
        END IF
      ELSE
        y_ => z_%parent%parent%left
        IF (y_%color .EQV. RED) THEN
          z_%parent%color = BLACK
          y_%color = BLACK
          z_%parent%parent%color = RED
          z_ => z_%parent%parent
        ELSE
          IF (ASSOCIATED(z_, z_%parent%left)) THEN
            z_ => z_%parent
            CALL right_rotate_(root_, z_)
          END IF
          z_%parent%color = BLACK
          z_%parent%parent%color = RED
          CALL left_rotate_(root_, z_%parent%parent)
        END IF
      END IF
    END DO
    root_%color = BLACK

  END SUBROUTINE insert_fixup_
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  SUBROUTINE delete_(root_, z_)
    IMPLICIT NONE

    TYPE(RBNODE_T), POINTER, INTENT(INOUT) :: root_
    TYPE(RBNODE_T), POINTER, INTENT(INOUT) :: z_
    TYPE(RBNODE_T), POINTER :: x_, y_
    LOGICAL :: y_color_

    y_ => z_
    y_color_ = y_%color

    IF (ASSOCIATED(z_%left, NIL)) THEN
      x_ => z_%right
      CALL transplant_(root_, z_, z_%right)
    ELSE IF (ASSOCIATED(z_%right, NIL)) THEN
      x_ => z_%left
      CALL transplant_(root_, z_, z_%left)
    ELSE
      y_ => min_(z_%right)
      y_color_ = y_%color
      x_ => y_%right
      IF (ASSOCIATED(y_%parent, z_)) THEN
        x_%parent => y_
      ELSE
        CALL transplant_(root_, y_, y_%right)
        y_%right => z_%right
        y_%right%parent => y_
      END IF
      CALL transplant_(root_, z_, y_)
      y_%left => z_%left
      y_%left%parent => y_
      y_%color = z_%color
    END IF

    IF (y_color_ .EQV. BLACK) CALL delete_fixup_(root_, x_)

  END SUBROUTINE delete_
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  SUBROUTINE delete_fixup_(root_, x_)
    IMPLICIT NONE

    TYPE(RBNODE_T), POINTER, INTENT(INOUT) :: root_
    TYPE(RBNODE_T), POINTER, INTENT(INOUT) :: x_
    TYPE(RBNODE_T), POINTER :: w_

    DO WHILE ((.NOT. ASSOCIATED(x_, root_)) .AND. (x_%color .EQV. BLACK))
      IF (ASSOCIATED(x_, x_%parent%left)) THEN
        w_ => x_%parent%right
        IF (w_%color .EQV. RED) THEN
          w_%color = BLACK
          x_%parent%color = RED
          CALL left_rotate_(root_, x_%parent)
          w_ => x_%parent%right
        END IF
        IF ((w_%left%color .EQV. BLACK) .AND. (w_%right%color .EQV. BLACK)) THEN
          w_%color = RED
          x_ => x_%parent
        ELSE
          IF (w_%right%color .EQV. BLACK) THEN
            w_%left%color = BLACK
            w_%color = RED
            CALL right_rotate_(root_, w_)
            w_ => x_%parent%right
          END IF
          w_%color = x_%parent%color
          x_%parent%color = BLACK
          w_%right%color = BLACK
          CALL left_rotate_(root_, x_%parent)
          x_ => root_
        END IF
      ELSE
        w_ => x_%parent%left
        IF (w_%color .EQV. RED) THEN
          w_%color = BLACK
          x_%parent%color = RED
          CALL right_rotate_(root_, x_%parent)
          w_ => x_%parent%left
        END IF
        IF ((w_%right%color .EQV. BLACK) .AND. (w_%left%color .EQV. BLACK)) THEN
          w_%color = RED
          x_ => x_%parent
        ELSE
          IF (w_%left%color .EQV. BLACK) THEN
            w_%right%color = BLACK
            w_%color = RED
            CALL left_rotate_(root_, w_)
            w_ => x_%parent%left
          END IF
          w_%color = x_%parent%color
          x_%parent%color = BLACK
          w_%left%color = BLACK
          CALL right_rotate_(root_, x_%parent)
          x_ => root_
        END IF
      END IF
    END DO

    x_%color = BLACK

  END SUBROUTINE delete_fixup_
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  SUBROUTINE left_rotate_(root_, xp_)
    IMPLICIT NONE

    TYPE(RBNODE_T), POINTER, INTENT(INOUT) :: root_
    TYPE(RBNODE_T), POINTER, INTENT(INOUT) :: xp_
    TYPE(RBNODE_T), POINTER :: x_, y_

    x_ => xp_
    y_ => x_%right

    CALL transplant_(root_, x_, y_)

    x_%right => y_%left
    IF (.NOT. ASSOCIATED(y_%left, NIL)) y_%left%parent => x_
    y_%left => x_
    x_%parent => y_

  END SUBROUTINE left_rotate_
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  SUBROUTINE right_rotate_(root_, yp_)
    IMPLICIT NONE

    TYPE(RBNODE_T), POINTER, INTENT(INOUT) :: root_
    TYPE(RBNODE_T), POINTER, INTENT(INOUT) :: yp_
    TYPE(RBNODE_T), POINTER :: x_, y_

    y_ => yp_
    x_ => y_%left

    CALL transplant_(root_, y_, x_)

    y_%left => x_%right
    IF (.NOT. ASSOCIATED(x_%right, NIL)) x_%right%parent => y_
    x_%right => y_
    y_%parent => x_

  END SUBROUTINE right_rotate_
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  SUBROUTINE transplant_(root_, up_, vp_)
    IMPLICIT NONE

    TYPE(RBNODE_T), POINTER, INTENT(INOUT) :: root_
    TYPE(RBNODE_T), POINTER, INTENT(INOUT) :: up_
    TYPE(RBNODE_T), POINTER, INTENT(INOUT) :: vp_
    TYPE(RBNODE_T), POINTER :: u_, v_

    u_ => up_
    v_ => vp_
    IF (ASSOCIATED(u_%parent, NIL)) THEN
      root_ => v_
    ELSE IF (ASSOCIATED(u_, u_%parent%left)) THEN
      u_%parent%left => v_
    ELSE
      u_%parent%right => v_
    END IF
    v_%parent => u_%parent

  END SUBROUTINE transplant_
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  FUNCTION min_(x_)
    ! find the minimal node on subtree whose root is 'x_'
    IMPLICIT NONE

    TYPE(RBNODE_T), POINTER, INTENT(IN) :: x_
    TYPE(RBNODE_T), POINTER :: min_

    min_ => x_
    DO WHILE (.NOT. ASSOCIATED(min_%left, NIL))
      min_ => min_%left
    END DO

    RETURN
  END FUNCTION min_
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  FUNCTION find_(node_, key_)
    IMPLICIT NONE

    TYPE(RBNODE_T), POINTER, INTENT(IN) :: node_
    REAL(8), INTENT(IN) :: key_
    TYPE(RBNODE_T), POINTER :: find_
    TYPE(RBNODE_T), POINTER :: p_

    p_ => node_
    find_ => NIL
    DO WHILE(.NOT. ASSOCIATED(p_, NIL))
      IF (key_ < p_%key) THEN
        p_ => p_%left
      ELSE IF (key_ > p_%key) THEN
        p_ => p_%right
      ELSE
        find_ => p_
        RETURN
      END IF
    END DO

    RETURN
  END FUNCTION find_
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  FUNCTION new_node_(key_, val_, color_)
    IMPLICIT NONE

    REAL(8), INTENT(IN) :: key_, val_
    LOGICAL, INTENT(IN) :: color_
    TYPE(RBNODE_T), POINTER :: new_node_

    ALLOCATE(new_node_)
    new_node_%key = key_
    new_node_%val = val_
    new_node_%color = color_
    new_node_%parent => NIL
    new_node_%left => NIL
    new_node_%right => NIL

  END FUNCTION new_node_
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  RECURSIVE SUBROUTINE free_node_(node_)
    IMPLICIT NONE

    TYPE(RBNODE_T), POINTER, INTENT(INOUT) :: node_

    IF (ASSOCIATED(node_%left)) THEN
      CALL free_node_(node_%left)
      DEALLOCATE(node_%left)
    END IF

    IF (ASSOCIATED(node_%right)) THEN
      CALL free_node_(node_%right)
      DEALLOCATE(node_%right)
    END IF

  END SUBROUTINE free_node_
  !----------------------------------------------------------------------------

END MODULE lib_rbt
